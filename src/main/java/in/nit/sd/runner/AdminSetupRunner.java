package in.nit.sd.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import in.nit.sd.constant.MyStatus;
import in.nit.sd.constant.UserRoles;
import in.nit.sd.entity.User;
import in.nit.sd.repository.UserRepository;
import in.nit.sd.service.UserService;

@Component
public class AdminSetupRunner implements CommandLineRunner{

	@Value("${admin.email}")
	private String username;
	
	@Value("${admin.address}")
	private String address;
	
	@Autowired
	private UserRepository repository;
	
	@Autowired
	private UserService service;
	
	
	@Override
	public void run(String... args) throws Exception {
		
		if(!repository.findByEmail(username).isPresent()) {
			User user = new User();
			user.setDisplayName("ADMIN");
			user.setEmail(username);
			user.setRoles(UserRoles.ADMIN);
			user.setStatus(MyStatus.ACTIVE);
			user.setAddress(address);
			service.saveUser(user);
		}
		
	}

	
}
