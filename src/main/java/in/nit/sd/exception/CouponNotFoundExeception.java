package in.nit.sd.exception;

public class CouponNotFoundExeception extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CouponNotFoundExeception() {
		super();
		
	}

	public CouponNotFoundExeception(String message) {
		super(message);
		
	}
	
	

}
