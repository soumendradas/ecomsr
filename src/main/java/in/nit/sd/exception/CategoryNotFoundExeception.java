package in.nit.sd.exception;

public class CategoryNotFoundExeception extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CategoryNotFoundExeception() {
		super();
		
	}

	public CategoryNotFoundExeception(String message) {
		super(message);
		
	}
	
	

}
