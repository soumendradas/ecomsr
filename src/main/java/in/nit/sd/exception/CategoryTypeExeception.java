package in.nit.sd.exception;

public class CategoryTypeExeception extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CategoryTypeExeception() {
		super();
		
	}

	public CategoryTypeExeception(String message) {
		super(message);
	}
	
	

}
