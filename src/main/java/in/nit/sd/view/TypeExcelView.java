package in.nit.sd.view;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import in.nit.sd.entity.CategoryType;

public class TypeExcelView extends AbstractXlsxView{

	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			Workbook workbook, 
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		response.addHeader("Content-Disposition", "attachment;filename=CategoryType.xlsx");
		
		Sheet sheet = workbook.createSheet("CategoryType Data");
		
		@SuppressWarnings("unchecked")
		List<CategoryType> list = (List<CategoryType>) model.get("list");
		
		createHeader(sheet);
		createBody(sheet, list);
		
	}

	private void createBody(Sheet sheet, List<CategoryType> list) {
		
		int rowNum = 1;
		for(CategoryType type: list) {
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(type.getId());
			row.createCell(1).setCellValue(type.getName());
			row.createCell(2).setCellValue(type.getStatus());
			row.createCell(3).setCellValue(type.getNote());
		}
		
	}

	private void createHeader(Sheet sheet) {
		
		Row row = sheet.createRow(0);
		row.createCell(0).setCellValue("ID");
		row.createCell(1).setCellValue("NAME");
		row.createCell(2).setCellValue("STATUS");
		row.createCell(3).setCellValue("NOTE");
	}

}
