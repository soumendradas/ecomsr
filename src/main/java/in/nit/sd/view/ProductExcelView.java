package in.nit.sd.view;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import in.nit.sd.entity.Product;

public class ProductExcelView extends AbstractXlsxView{

	@Override
	protected void buildExcelDocument(Map<String, Object> model, 
			Workbook workbook,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		response.addHeader("Content-Disposition", "attachment;filename=Products.xlsx");
		
		@SuppressWarnings("unchecked")
		List<Product> list = (List<Product>) model.get("list");
		Sheet sheet = workbook.createSheet("ProductSheet");
		createHeader(sheet);
		createBody(sheet, list);
	}

	private void createBody(Sheet sheet, List<Product> list) {
		int rowNum = 1;
		for(Product p: list) {
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(p.getId());
			row.createCell(1).setCellValue(p.getName());
			row.createCell(2).setCellValue(p.getStatus());
			row.createCell(3).setCellValue(p.getInStock());
			row.createCell(4).setCellValue(p.getCost());
			row.createCell(5).setCellValue(p.getBrand().getName());
			row.createCell(6).setCellValue(p.getCategory().getName());
		}
		
	}

	private void createHeader(Sheet sheet) {
		Row row = sheet.createRow(0);
		row.createCell(0).setCellValue("ID");
		row.createCell(1).setCellValue("NAME");
		row.createCell(2).setCellValue("STATUS");
		row.createCell(3).setCellValue("IN_STOCK");
		row.createCell(4).setCellValue("COST");
		row.createCell(5).setCellValue("BRAND");
		row.createCell(6).setCellValue("CATEGORY");
	}

}
