package in.nit.sd.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.springframework.stereotype.Component;

import in.nit.sd.constant.OrderStatus;
import in.nit.sd.entity.CartItem;
import in.nit.sd.entity.Order;
import in.nit.sd.entity.OrderItem;

@Component
public class OrderUtil {
	
	public Order MapCartItemsToOrder(List<CartItem> cartItems) {
		Order order = new Order();
		order.setStatus(OrderStatus.OPEN);
		
		List<OrderItem> orderItems = new ArrayList<>();
		
		for(CartItem item: cartItems) {
			OrderItem orderItem = new OrderItem();
			orderItem.setProduct(item.getProduct());
			orderItem.setQty(item.getQty());
			orderItem.setLineAmount(item.getQty() * item.getProduct().getCost());
			
			orderItems.add(orderItem);
		}
		
		order.setOrderItems(orderItems);
		
		return order;
	}
	
	public void calculateGrandTotal(List<Order> orders) {
		
		for(Order order: orders) {
			
			Double amount = order.getOrderItems().stream()
					.mapToDouble(ob->ob.getLineAmount()).sum();
			
			order.setGrandTotal(amount);
		}
	}
	
	public void generatePie(List<Object[]> data,String path) {
		
		
		DefaultPieDataset<String> dataset = new DefaultPieDataset<>();
		
		for(Object[] ob: data) {
			dataset.setValue(ob[0].toString(), Double.valueOf(ob[1].toString()));
		}
		
		JFreeChart chart = ChartFactory.createPieChart("ORDER STATUS CHART", dataset);
		
		try {
			ChartUtils.saveChartAsJPEG(new File(path+"/orderA.jpg"), chart, 400, 400);
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void generateBar(List<Object[]> data, String path) {
		
		//Create Dataset
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		
		for(Object[] ob:data) {
			dataset.setValue(Double.valueOf(ob[1].toString()), ob[0].toString(), "");
		}
		
		//Create JfreeChart object using ChartFactory
		JFreeChart chart = ChartFactory.createBarChart(
				"ORDER STATUS", "STATUS CODE", "COUNT", dataset);
		
		try {
			ChartUtils.saveChartAsJPEG(new File(path+"/orderB.jpg"), chart, 400, 400);
		}catch (IOException e) {
			e.printStackTrace();
		}
	}

}
