package in.nit.sd.util;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class AppUtil {
	
	public static Map<Long, String> convertListToMap(List<Object[]> list){
		
		return list.stream().collect(
				Collectors.toMap(ob->Long.valueOf(ob[0].toString()),
						ob-> ob[1].toString()));
	}
	
	public static String genPwd() {
		
		return UUID.randomUUID().
				toString().replace("-", "").substring(0,8);
	}
	
	public static boolean isUserLoggedIn(Principal p) {
		
		return (p!=null && p.getName()!=null)? true: false;
	}

}
