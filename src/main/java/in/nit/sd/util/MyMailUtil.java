package in.nit.sd.util;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class MyMailUtil {

	@Autowired
	private JavaMailSender mailSender;
	
	public boolean send(
			String to[],
			String cc[],
			String bcc[],
			String subject,
			String text,
			Resource files[]
			) {
		
		boolean sent = false;
		
		try {
			//1. Create new empty message object
			MimeMessage message = mailSender.createMimeMessage();
			
			//2. Fill details (message, attachmentExist)
			MimeMessageHelper helper = new MimeMessageHelper(message, 
					files != null&& files.length>0);
			
			helper.setTo(to);
			if(cc!=null && cc.length > 0) {
				helper.setCc(cc);
			}
			
			if(bcc!=null && bcc.length > 0) {
				helper.setBcc(bcc);
			}
			
			helper.setSubject(subject);
			helper.setText(text);
			
			if(files!= null && files.length > 0) {
				for(Resource file: files) {
					helper.addAttachment(file.getFilename(), file);
				}
			}
			
			mailSender.send(message);
			sent = true;
			
		}catch (Exception e) {
			e.printStackTrace();
			sent = false;
		}
		
		return sent;
	}
	
	public boolean send(String to,
						String subject,
						String text) {
		
		return send(new String[] {to}, null, null, subject, text, null);
	}
}
