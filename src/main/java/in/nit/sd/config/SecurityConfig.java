package in.nit.sd.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import in.nit.sd.constant.UserRoles;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Bean
	public PasswordEncoder encoder() {
		
		return new BCryptPasswordEncoder();
	}
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService)
			.passwordEncoder(encoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.sessionManagement().maximumSessions(1).maxSessionsPreventsLogin(true);
		http.authorizeRequests()
			.mvcMatchers("/", "/search/**").permitAll()
			.mvcMatchers("user/login", "customer/register", "customer/save").permitAll()
			.mvcMatchers("/order/all","/order/updateOrderStatus", "/order/reports")
				.hasAuthority(UserRoles.SALES.name())
			.mvcMatchers("/emp/**").hasAuthority(UserRoles.EMPLOYEE.name())
			.mvcMatchers("/sales/**").hasAuthority(UserRoles.SALES.name())
			.mvcMatchers("/order/**").hasAuthority(UserRoles.CUSTOMER.name())
			.mvcMatchers("/cart/**").hasAuthority(UserRoles.CUSTOMER.name())
			.mvcMatchers("customer/all").hasAuthority(UserRoles.ADMIN.name())
			.mvcMatchers("/coupon/**", "/product/**", "/shipping/**", "/stock/**")
				.hasAuthority(UserRoles.SALES.name())
			.mvcMatchers("/brand/**", "/category/**", "/categorytype/**")
				.hasAuthority(UserRoles.EMPLOYEE.name())
			.mvcMatchers("/user/setup").authenticated()
			.mvcMatchers("/user/**")
				.hasAnyAuthority(UserRoles.ADMIN.name())
			.anyRequest().authenticated()
			.and()
			.formLogin()
			.loginPage("/user/login") 	//show login page
			.loginProcessingUrl("/login") //todo login check
			.defaultSuccessUrl("/user/setup", true)
			.failureUrl("/user/login?error=true")
			
			.and()
			.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			.logoutSuccessUrl("/user/login?logout=true");
	}

}
