package in.nit.sd.service;

import java.util.List;
import java.util.Map;

import in.nit.sd.entity.Brand;

public interface BrandService {
	
	Long saveBrand(Brand brand);
	List<Brand> getAllBrand();
	Brand getOneBrand(Long id);
	void deleteBrand(Long id);
	String updateBrand(Brand brand);
	
	Map<Long, String> getAllBrandNameAndId();
	
	boolean isBrandNameExist(Long id, String name);
	boolean isBrandCodeExist(Long id, String name);
	
	List<Object[]> getBrandIdAndImageLink();
	
	long totalBrands();
}
