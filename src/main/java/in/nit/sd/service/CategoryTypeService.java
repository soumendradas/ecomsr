package in.nit.sd.service;

import java.util.List;
import java.util.Map;

import in.nit.sd.entity.CategoryType;

public interface CategoryTypeService {
	
	Long saveCategoryType(CategoryType categorytype);
	void updateCategoryType(CategoryType categorytype);
	void deleteCategoryType(Long id);
	CategoryType getOneCategoryType(Long id);
	List<CategoryType> getAllCategoryTypes();
	
	Map<Long,String> getCategoryTypeIdAndName();
	
	long totalCategoryTypes();

}
