package in.nit.sd.service;

import java.util.List;

import in.nit.sd.entity.Address;
import in.nit.sd.entity.Customer;

public interface CustomerService {
	
	Long saveCustomer(Customer customer);
	List<Customer> getAllCustomers();
	Customer getOneCustomer(Long id);
	void updateCustomer(Customer customer);
	
	Customer findByEmail(String email);
	
	List<Address> getCustomerAddress(Long custId);
	
}
