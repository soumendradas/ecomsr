package in.nit.sd.service;

import java.util.List;

import in.nit.sd.entity.CartItem;

public interface CartItemService {
	
	Long addCartItem(CartItem cartItem);
	void removeCartitem(Long cartItemId);
	
	List<CartItem> viewAllItems(Long custId);
	CartItem getOneCartItem(Long custId,Long prodId);
	void updateQty(Long cartItemId,Integer qty);
	
	int getCartItemCount(Long custId);
	
	void deleteAllCartItems(List<CartItem> items);

}
