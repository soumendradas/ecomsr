package in.nit.sd.service;

import java.util.List;

import in.nit.sd.entity.Shipping;

public interface ShippingService {
	
	Long saveShipping(Shipping shipping);
	List<Shipping> getAllShipping();
	Shipping getOneShipping(Long id);
	void updateShipping(Shipping shipping);
	void deleteShipping(Long id);
}
