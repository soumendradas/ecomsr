package in.nit.sd.service;

import java.util.List;

import in.nit.sd.entity.Coupon;

public interface CouponService {
	
	Long saveCoupon(Coupon coupon);
	List<Coupon> getAllCoupon();
	Coupon getOneCoupon(Long id);
	
	void deleteCoupon(Long id);
	String updateCoupon(Coupon coupon);
}
