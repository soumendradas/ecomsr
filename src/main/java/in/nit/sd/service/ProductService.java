package in.nit.sd.service;

import java.util.List;
import java.util.Map;

import in.nit.sd.entity.Product;

public interface ProductService {
	
	Long saveProduct(Product product);
	List<Product> getAllProducts();
	Product getOneProduct(Long id);
	void updateProduct(Product product);
	void deleteProduct(Long id);
	
	Map<Long, String> getAllProductIdAndName();
	
	List<Object[]> getProductByBrandId(Long brandId);
	
	List<Object[]> getProductByCategoryId(Long catId);
	
	List<Object[]> getProductByMatchingName(String name);
	
	long totalProducts();
}
