package in.nit.sd.service;

import java.util.List;

import in.nit.sd.constant.MyStatus;
import in.nit.sd.entity.User;

public interface UserService {
	Long saveUser(User user);
	List<User> getAllUser();
	User getOneUser(Long id);
	User findByEmail(String email);
	
	void updateStatus(Long id, MyStatus status);
	
	void updateByAdmin(User user);
	boolean checkEmail(Long id, String email);
}
