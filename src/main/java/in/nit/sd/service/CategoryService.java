package in.nit.sd.service;

import java.util.List;
import java.util.Map;

import in.nit.sd.entity.Category;

public interface CategoryService {
	
	Long saveCategory(Category category);
	void updateCategory(Category category);
	void deleteCategory(Long id);
	Category getOneCategory(Long id);
	List<Category> getAllCategorys();
	
	Map<Long, String> getAllCategoryIdAndName();
	
	List<Category> getCategoryByType(Long typeID);
	
	long totalCategories();


}
