package in.nit.sd.service;

import java.util.List;

import in.nit.sd.constant.OrderStatus;
import in.nit.sd.entity.Order;

public interface OrderService {
	
	Long placeOrder(Order order);
	List<Order> getOrdersByCustomerId(Long id);
	List<Order> fetchAllOrders();
	void updateOrderStatus(Long id,OrderStatus status);
	List<Order> findByOrderStatus(OrderStatus status);
	
	List<Order> getAllOrder();
	
	List<Object[]> getOrderStatusAndCount();

}
