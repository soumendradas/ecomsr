package in.nit.sd.service;

import java.util.List;

import in.nit.sd.entity.Stock;

public interface StockService {
	Long createStock(Stock stock);
	void updateStock(Long id, Integer count);
	Long getStockIdByProduct(Long productId);
	List<Stock> getStockDetails();
	Stock getOneStock(Long id);
}
