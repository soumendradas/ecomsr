package in.nit.sd.service.impl;


import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import in.nit.sd.constant.MyStatus;
import in.nit.sd.entity.User;
import in.nit.sd.exception.UserNotFoundException;
import in.nit.sd.repository.UserRepository;
import in.nit.sd.service.UserService;
import in.nit.sd.util.AppUtil;
import in.nit.sd.util.MyMailUtil;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

	@Autowired
	private UserRepository repo;
	
	@Autowired
	private PasswordEncoder encoder;
	
	@Autowired
	private MyMailUtil mailUtil;
	
	@Override
	public Long saveUser(User user) {
		
		String password = AppUtil.genPwd();
		user.setPassword(encoder.encode(password));
		
		Long id = repo.save(user).getId();
		
		if(id!=null) {
			new Thread(()->{
				String text = "User created Successfull and username is : "
							+user.getEmail()+" and password : "+ password+ 
							" with Role "+ user.getRoles().name();
				mailUtil.send(user.getEmail(), "User Created", text);
			}).start();
		}
		
		return id;
	}

	@Override
	public List<User> getAllUser() {
		
		return repo.findAll();
	}

	@Override
	public User getOneUser(Long id) {
		
		return repo.findById(id).orElseThrow(
				()-> new UserNotFoundException("nd"));
	}

	@Override
	public User findByEmail(String email) {
		
		return repo.findByEmail(email).orElseThrow(
				()-> new UserNotFoundException("Email invalid"));
	}
	
	@Override
	@Transactional
	public void updateStatus(Long id, MyStatus status) {
		
		if(repo.existsById(id)) {
			repo.updateStatus(id, status);
		}else {
			throw new UserNotFoundException("User ID: "+id+" not Found");
		}
		
	}
	
	@Override
	@Transactional
	public void updateByAdmin(User user) {
		
		if(repo.existsById(user.getId())) {
			repo.updateByAdmin(user.getId(), user.getDisplayName(),
					user.getEmail(), user.getRoles(), user.getAddress());
		}else {
			throw new UserNotFoundException("User Not Found");
		}
		
	}

	@Override
	public UserDetails loadUserByUsername(String username) 
			throws UsernameNotFoundException {
		User user = repo.findByEmail(username).
				orElseThrow(()-> new UsernameNotFoundException("User Not Found"));
		
		boolean status = user.getStatus()==MyStatus.ACTIVE?true:false;
		
		return new org.springframework.security.core.userdetails
				.User(user.getEmail(), user.getPassword(),
						status, true, true, true, Arrays.asList(
								new SimpleGrantedAuthority(user.getRoles().name())));
	}
	
	@Override
	public boolean checkEmail(Long id, String email) {
		
		if(id != 0) {
			return repo.countEmail(id, email)>0;
		}
		
		return repo.countEmail(email)>0;
	}
}
