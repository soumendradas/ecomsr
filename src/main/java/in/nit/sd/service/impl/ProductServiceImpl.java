package in.nit.sd.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.sd.constant.MyStatus;
import in.nit.sd.entity.Product;
import in.nit.sd.exception.ProductNotFoundException;
import in.nit.sd.repository.ProductRepository;
import in.nit.sd.service.ProductService;
import in.nit.sd.util.AppUtil;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository repo;

	@Override
	public Long saveProduct(Product product) {
		
		return repo.save(product).getId();
	}

	@Override
	public List<Product> getAllProducts() {
		
		return repo.findAll();
	}

	@Override
	public Product getOneProduct(Long id) {
		
		return repo.findById(id).orElseThrow(
				()->new ProductNotFoundException("Id invalid"));
	}

	@Override
	public void updateProduct(Product product) {
		if(repo.existsById(product.getId()) || product.getId()!= null) {
			repo.save(product);
		}else {
			throw new ProductNotFoundException("Id is Invalid");
		}
	}

	@Override
	public void deleteProduct(Long id) {
		repo.delete(getOneProduct(id));
		
	}
	
	@Override
	public Map<Long, String> getAllProductIdAndName() {
		List<Object[]> list = repo.getAllIdAndName(MyStatus.ACTIVE.name());
		return AppUtil.convertListToMap(list);
	}

	@Override
	public List<Object[]> getProductByBrandId(Long brandId) {
		
		return repo.getAllProductByBrand(brandId);
	}
	
	@Override
	public List<Object[]> getProductByCategoryId(Long catId) {
		
		return repo.getAllProductByCategory(catId);
	}
	
	@Override
	public List<Object[]> getProductByMatchingName(String name) {
		
		return repo.getProductBymatchingName(name);
	}
	
	@Override
	public long totalProducts() {
		
		return repo.count();
	}

}
