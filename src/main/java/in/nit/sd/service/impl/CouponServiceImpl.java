package in.nit.sd.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.sd.entity.Coupon;
import in.nit.sd.exception.CouponNotFoundExeception;
import in.nit.sd.repository.CouponRepository;
import in.nit.sd.service.CouponService;

@Service
public class CouponServiceImpl implements CouponService{
	
	@Autowired
	private CouponRepository repo;

	@Override
	public Long saveCoupon(Coupon coupon) {
		
		return repo.save(coupon).getId();
	}

	@Override
	public List<Coupon> getAllCoupon() {
		
		return repo.findAll();
	}

	@Override
	public Coupon getOneCoupon(Long id) {
		
		return repo.findById(id).orElseThrow(
				()->new CouponNotFoundExeception("Id Not found"));

	}

	@Override
	public void deleteCoupon(Long id) {
		
		repo.delete(getOneCoupon(id));
		
	}

	@Override
	public String updateCoupon(Coupon coupon) {
		
		if(repo.existsById(coupon.getId())) {
			repo.save(coupon);
			return "Coupon Id "+coupon.getId()+" is updated";
		}
		return "Coupon Id '"+coupon.getId()+"' not found";
		
	}
	
	
}
