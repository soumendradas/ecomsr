package in.nit.sd.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.sd.entity.Stock;
import in.nit.sd.exception.StockNotFoundException;
import in.nit.sd.repository.StockRepository;
import in.nit.sd.service.StockService;

@Service
public class StockServiceImpl implements StockService{
	
	@Autowired
	private StockRepository repo;

	
	public Long createStock(Stock stock) {
		
		stock.setQoh(stock.getCount());
		
		return repo.save(stock).getId();
	}

	
	@Transactional
	public void updateStock(Long id, Integer count) {
		
		if(id == null || !repo.existsById(id)) {
			throw new StockNotFoundException("Stock id not available");
		}else {
			repo.updateStock(id, count);;
		}
		
	}


	public Long getStockIdByProduct(Long productId) {
		
		return repo.getStockIdByProductId(productId);
	}

	
	public List<Stock> getStockDetails() {
		
		return repo.findAll();
	}
	
	@Override
	public Stock getOneStock(Long id) {
		
		return repo.findById(id).orElseThrow(
				()-> new StockNotFoundException("Stock not found"));
	}
	
	
}
