package in.nit.sd.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.sd.constant.OrderStatus;
import in.nit.sd.entity.Order;
import in.nit.sd.repository.OrderRepository;
import in.nit.sd.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderRepository repo;
	
	@Override
	public Long placeOrder(Order order) {
		
		return repo.save(order).getId();
	}

	@Override
	public List<Order> getOrdersByCustomerId(Long id) {
		
		return repo.getOrdersByCustomer(id);
	}

	@Override
	public List<Order> fetchAllOrders() {
		
		return repo.findAll();
	}

	@Override
	public List<Order> findByOrderStatus(OrderStatus status) {
		
		return repo.findByStatus(status);
	}
	
	@Override
	@Transactional
	public void updateOrderStatus(Long id, OrderStatus status) {
		
		if(repo.existsById(id)) {
			repo.updateOrderStatus(id, status);
		}
	}
	
	@Override
	public List<Order> getAllOrder() {
		
		return repo.findAll();
	}
	
	@Override
	public List<Object[]> getOrderStatusAndCount() {
		
		return repo.getOrderStatusAndCount();
	}

}
