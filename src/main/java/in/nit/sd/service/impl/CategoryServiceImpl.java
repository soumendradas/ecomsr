package in.nit.sd.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.sd.entity.Category;
import in.nit.sd.exception.CategoryNotFoundExeception;
import in.nit.sd.repository.CategoryRepository;
import in.nit.sd.service.CategoryService;
import in.nit.sd.util.AppUtil;

@Service
public class CategoryServiceImpl implements CategoryService{
	
	@Autowired
	private CategoryRepository repo;

	@Override
	public Long saveCategory(Category category) {
		
		return repo.save(category).getId();
	}

	@Override
	public void updateCategory(Category category) {
		if(repo.existsById(category.getId())) {
			repo.save(category);
		}
		
	}

	@Override
	public void deleteCategory(Long id) {
		repo.delete(getOneCategory(id));
		
	}

	@Override
	public Category getOneCategory(Long id) {
		
		return repo.findById(id).orElseThrow(
				()->new CategoryNotFoundExeception("Id is invalid"));
	}

	@Override
	public List<Category> getAllCategorys() {
		
		return repo.findAll();
	}

	@Override
	public Map<Long, String> getAllCategoryIdAndName() {
		List<Object[]> list = repo.getAllCategoryIdAndName();
		return AppUtil.convertListToMap(list);
	}
	
	@Override
	public List<Category> getCategoryByType(Long typeID) {
		
		return repo.getCategoryByCategoryType(typeID);
	}
	
	@Override
	public long totalCategories() {
		
		return repo.count();
	}

}
