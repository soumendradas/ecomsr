package in.nit.sd.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.sd.constant.MyStatus;
import in.nit.sd.constant.UserRoles;
import in.nit.sd.entity.Address;
import in.nit.sd.entity.Customer;
import in.nit.sd.entity.User;
import in.nit.sd.exception.CustomerNotFoundException;
import in.nit.sd.repository.CustomerRepository;
import in.nit.sd.service.CustomerService;
import in.nit.sd.service.UserService;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	@Autowired
	private CustomerRepository repo;
	
	@Autowired
	private UserService userService;

	@Override
	public Long saveCustomer(Customer customer) {
		
		Long id = repo.save(customer).getId();
		
		if(id!=null) {
			User user = new User();
			user.setDisplayName(customer.getName());
			user.setEmail(customer.getEmail());
			user.setAddress(customer.getAddress().get(0).toString());
			user.setStatus(MyStatus.ACTIVE);
			user.setRoles(UserRoles.CUSTOMER);
			
			userService.saveUser(user);
		}
		
		return id;
	}

	@Override
	public List<Customer> getAllCustomers() {
		
		return repo.findAll();
	}

	@Override
	public Customer getOneCustomer(Long id) {
		
		return repo.findById(id).orElseThrow(
				()-> new CustomerNotFoundException("Customer ID not found")
				);
	}

	@Override
	public void updateCustomer(Customer customer) {
		
		if(customer.getId()!=null && !repo.existsById(customer.getId())) {
			repo.save(customer);
		}
		throw new CustomerNotFoundException("ID not Found");
		
	}
	
	@Override
	public Customer findByEmail(String email) {
		
		return repo.findByEmail(email)
				.orElseThrow(()-> new CustomerNotFoundException("Email Invalid"));
	}
	
	@Override
	public List<Address> getCustomerAddress(Long custId) {
		
		return repo.getCustomerAddresses(custId);
	}
}
