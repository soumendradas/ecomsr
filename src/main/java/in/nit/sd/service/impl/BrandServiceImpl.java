package in.nit.sd.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.sd.entity.Brand;
import in.nit.sd.exception.BrandNotFoundException;
import in.nit.sd.repository.BrandRepository;
import in.nit.sd.service.BrandService;
import in.nit.sd.util.AppUtil;

@Service
public class BrandServiceImpl implements BrandService{

	@Autowired
	private BrandRepository repo;
	
	@Override
	public Long saveBrand(Brand brand) {
		
		return repo.save(brand).getId();
	}

	@Override
	public List<Brand> getAllBrand() {
		
		return repo.findAll();
	}

	@Override
	public Brand getOneBrand(Long id) {
		
		return repo.findById(id).orElseThrow(
				()->new BrandNotFoundException("ID IS INVALID"));
	}

	@Override
	public void deleteBrand(Long id) {
		repo.delete(getOneBrand(id));
		
	}

	@Override
	public String updateBrand(Brand brand) {
		
		if(repo.existsById(brand.getId())) {
			repo.save(brand);
			return "Brand updated with ID: "+brand.getId();
		}
		
		return null;
	}
	
	@Override
	public Map<Long, String> getAllBrandNameAndId() {
		List<Object[]> list = repo.getAllBrandIdAndName();
		return AppUtil.convertListToMap(list);
	}
	
	@Override
	public boolean isBrandNameExist(Long id, String name) {
		
		if(id!=0) {
			return repo.getBrandNameCountForEdit(id, name)>0;
		}
		
		return repo.getBrandNameCount(name) > 0;
	}
	
	@Override
	public boolean isBrandCodeExist(Long id, String code) {
		
		if(id!=0) {
			return repo.getBrandCodeCountForEdit(id, code)>0;
		}
		return repo.getBrandCodeCount(code)>0;
	}
	
	@Override
	public List<Object[]> getBrandIdAndImageLink() {
		
		return repo.getBrandIdAndImageLink();
	}
	
	@Override
	public long totalBrands() {
		
		return repo.count();
	}

}
