package in.nit.sd.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.sd.entity.Shipping;
import in.nit.sd.exception.ShippingNotFoundException;
import in.nit.sd.repository.ShippingRepository;
import in.nit.sd.service.ShippingService;

@Service
public class ShippingServiceImpl implements ShippingService{
	
	@Autowired
	private ShippingRepository repo;

	@Override
	public Long saveShipping(Shipping shipping) {
		
		return repo.save(shipping).getId();
	}

	@Override
	public List<Shipping> getAllShipping() {
		
		return repo.findAll();
	}

	@Override
	public Shipping getOneShipping(Long id) {
		
		return repo.findById(id).orElseThrow(
				()-> new ShippingNotFoundException("ID"+ id +" is invalid"));
	}

	@Override
	public void updateShipping(Shipping shipping) {
		if(shipping.getId() == null || !repo.existsById(shipping.getId())) {
			
			throw new ShippingNotFoundException("Shipping ID "+ shipping.getId()+" is invalid");
		}else {
			repo.save(shipping);
		}
		
	}

	@Override
	public void deleteShipping(Long id) {
		
		repo.delete(getOneShipping(id));
		
	}

}
