package in.nit.sd.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.sd.entity.CartItem;
import in.nit.sd.exception.CartItemNotFoundException;
import in.nit.sd.repository.CartItemRepository;
import in.nit.sd.service.CartItemService;

@Service
public class CartItemServiceImpl implements CartItemService{
	
	@Autowired
	private CartItemRepository repo;

	@Override
	public Long addCartItem(CartItem cartItem) {
		
		return repo.save(cartItem).getId();
	}

	@Override
	public void removeCartitem(Long cartItemId) {
		
		if(repo.existsById(cartItemId)) {
			repo.deleteById(cartItemId);
		}
		
	}

	@Override
	public List<CartItem> viewAllItems(Long custId) {
		
		return repo.getAllCartItem(custId);
	}

	@Override
	public CartItem getOneCartItem(Long custId, Long prodId) {
		Optional<CartItem> item = repo.getOneCartItemByCustIdAndProdId(prodId, custId);
		
		if(item.isPresent()) {
			return item.get();
		}else {
			return null;
		}
		
	}

	@Override
	@Transactional
	public void updateQty(Long cartItemId, Integer qty) {
		
		if(repo.existsById(cartItemId)&& cartItemId != null) {
			repo.updateQty(cartItemId, qty);
		}
		
	}
	
	@Override
	public int getCartItemCount(Long custId) {
		
		return repo.getCartItemsCount(custId);
	}
	
	
	@Override
	public void deleteAllCartItems(List<CartItem> items) {
		
		repo.deleteAll(items);
		
	}

}
