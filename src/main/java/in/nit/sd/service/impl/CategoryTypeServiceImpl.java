package in.nit.sd.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nit.sd.entity.CategoryType;
import in.nit.sd.exception.CategoryTypeExeception;
import in.nit.sd.repository.CategoryTypeRepository;
import in.nit.sd.service.CategoryTypeService;
import in.nit.sd.util.AppUtil;

@Service
public class CategoryTypeServiceImpl implements CategoryTypeService{
	
	@Autowired
	private CategoryTypeRepository repo;

	@Override
	public Long saveCategoryType(CategoryType categorytype) {
		
		return repo.save(categorytype).getId();
	}

	@Override
	public void updateCategoryType(CategoryType categorytype) {
		
		if(repo.existsById(categorytype.getId())) {
			repo.save(categorytype);
		}
	}

	@Override
	public void deleteCategoryType(Long id) {
		
		repo.delete(getOneCategoryType(id));
	}

	@Override
	public CategoryType getOneCategoryType(Long id) {
		
		return repo.findById(id).orElseThrow(()->
			new CategoryTypeExeception("Category Not Found of this "+id));
	}

	@Override
	public List<CategoryType> getAllCategoryTypes() {
		
		return repo.findAll();
	}

	@Override
	public Map<Long, String> getCategoryTypeIdAndName() {
		
		List<Object[]> list = repo.findCategoryTypeIdAndName();
		
		return AppUtil.convertListToMap(list);
	}
	
	@Override
	public long totalCategoryTypes() {
		
		return repo.count();
	}

}
