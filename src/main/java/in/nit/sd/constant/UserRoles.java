package in.nit.sd.constant;

public enum UserRoles {

	ADMIN, EMPLOYEE, SALES, CUSTOMER
}
