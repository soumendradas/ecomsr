package in.nit.sd.constant;

public enum OrderStatus {
	
	PLACED, ORDERED, SHIPPING, 
	OUTFORDELIVERY, DELIVERED,
	CANCELLED, RETURNED, OPEN

}
