package in.nit.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import in.nit.sd.entity.Brand;
import in.nit.sd.entity.Category;
import in.nit.sd.entity.CategoryType;
import in.nit.sd.service.BrandService;
import in.nit.sd.service.CategoryService;
import in.nit.sd.service.CategoryTypeService;
import in.nit.sd.view.BrandExcelView;
import in.nit.sd.view.CategoryExcelView;
import in.nit.sd.view.TypeExcelView;

@Controller
@RequestMapping("emp")
public class EmployReports {

	@Autowired
	private BrandService brandService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private CategoryTypeService typeService;
	
	@GetMapping("reports")
	public String showReports(Model model) {
		long brandsCount = brandService.totalBrands();
		long categoryCount = categoryService.totalCategories();
		long typesCount = typeService.totalCategoryTypes();
		
		model.addAttribute("brands", brandsCount);
		model.addAttribute("categories", categoryCount);
		model.addAttribute("types", typesCount);
		return "EmployeeReportPage";
	}
	
	@GetMapping("exportBrands")
	public ModelAndView exportBrands() {
		
		ModelAndView mv = new ModelAndView();
		mv.setView(new BrandExcelView());
		List<Brand> list = brandService.getAllBrand();
		mv.addObject("list", list);
		
		return mv;
	}
	
	@GetMapping("exportCategoryTypes")
	public ModelAndView exportCategoryType() {
		ModelAndView mv = new ModelAndView();
		mv.setView(new TypeExcelView());
		
		List<CategoryType> list = typeService.getAllCategoryTypes();
		mv.addObject("list", list);
		
		return mv;
	}
	
	@GetMapping("exportcategories")
	public ModelAndView exportCategories() {
		ModelAndView mv = new ModelAndView();
		mv.setView(new CategoryExcelView());
		List<Category> list = categoryService.getAllCategorys();
		mv.addObject("list",list);
		
		return mv;
	}
	
	
}
