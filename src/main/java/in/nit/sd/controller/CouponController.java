package in.nit.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.sd.entity.Coupon;
import in.nit.sd.service.CouponService;

@Controller
@RequestMapping("/coupon")
public class CouponController {
	
	@Autowired
	private CouponService service;
	
	@GetMapping("/register")
	public String showRegisterPage(
				@RequestParam(value = "message", required = false) String message,
				Model model) {
		
		model.addAttribute("message", message);
		
		return "CouponRegisterPage";
	}
	
	@PostMapping("/save")
	public String saveCoupon(@ModelAttribute Coupon coupon,
			RedirectAttributes attributes) {
		
		Long id = service.saveCoupon(coupon);
		
		String message = "Coupon Id "+id+" is created successfully";
		
		attributes.addAttribute("message", message);
		
		return "redirect:register";
	}
	
	@GetMapping("/all")
	public String showAllCouponData(Model model,
			@RequestParam(value = "message", required = false)String message) {
		
		List<Coupon> coupons = service.getAllCoupon();
		
		model.addAttribute("list", coupons);
		model.addAttribute("message", message);
		
		return "CouponData";
	}
	
	@GetMapping("/delete")
	public String deleteCoupon(@RequestParam Long id,
			RedirectAttributes attributes) {
		
		String message = null;
		
		try {
			service.deleteCoupon(id);
			message = "Coupon "+id+ " is deleted";
		}catch (Exception e) {
			message = e.getMessage();
		}
		
		attributes.addAttribute("message", message);
		
		return "redirect:all";
	}
	
	@GetMapping("/edit")
	public String showEditPage(@RequestParam Long id, Model model,
			RedirectAttributes attributes) {
		
		String page = null;
		
		try {
			Coupon coupon = service.getOneCoupon(id);
			model.addAttribute("coupon", coupon);
			page = "CouponEdit";
		}catch (Exception e) {
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		
		return page;
	}
	
	@PostMapping("/update")
	public String updateCoupon(@ModelAttribute Coupon coupon,
			RedirectAttributes attributes) {
		
		String message = service.updateCoupon(coupon);
		
		attributes.addAttribute("message", message);
		
		return "redirect:all";
	}
}
