package in.nit.sd.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.sd.entity.Category;
import in.nit.sd.exception.CategoryNotFoundExeception;
import in.nit.sd.service.CategoryService;
import in.nit.sd.service.CategoryTypeService;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/category")
@Slf4j
public class CategoryController {
	
	@Autowired
	private CategoryService service;
	
	@Autowired
	private CategoryTypeService catService;
	
	private void sendCategoryType(Model model) {
		Map<Long, String> cats = catService.getCategoryTypeIdAndName();
		model.addAttribute("cats", cats);
	}
	
	@GetMapping("/register")
	public String showRegisterPage(Model model,
			@RequestParam(value = "message", required = false)String message) {
		
		sendCategoryType(model);
		model.addAttribute("message", message);
		
		return "CategoryRegister";
	}
	
	@PostMapping("save")
	public String saveCategory(@ModelAttribute Category category,
			RedirectAttributes attributes) {
		log.info("ENTER TO SAVE METHOD");
		try {
			Long id = service.saveCategory(category);
			log.debug("Category created ID {}", id);
			attributes.addAttribute("message", "Category created id: "+id);
		}catch (Exception e) {
			log.error(e.getMessage());
		}
		
		log.info("Exist from save method");
		
		
		return "redirect:register";
	}
	
	@GetMapping("/all")
	public String showAll(Model model, 
			@RequestParam(value = "message", required = false) String message) {
		
		List<Category> list = service.getAllCategorys();
		model.addAttribute("list",list);
		model.addAttribute("message", message);
		
		return "CategoryData";
	}
	
	@GetMapping("delete")
	public String deleteCategory(@RequestParam Long id,
			RedirectAttributes attributes) {
		String message = null;
		
		log.info("Enter to delete method");
		
		try {
			service.deleteCategory(id);
			message = "Category deleted with id "+id;
			log.debug(message);
		}catch (CategoryNotFoundExeception e) {
			message = e.getMessage();
			log.error(message);
		}
		
		log.info("Exist from delete method");
		
		attributes.addAttribute("message", message);
		return "redirect:all";
	}
	
	@GetMapping("edit")
	public String showEditPage(@RequestParam Long id, Model model,
			RedirectAttributes attributes) {
		
		String page = null;
		log.info("Enter to showEdit method");
		
		try {
			Category category = service.getOneCategory(id);
			model.addAttribute("category", category);
			sendCategoryType(model);
			page = "CategoryEdit";
			log.debug("Get Category data successfully");
			
		}catch (CategoryNotFoundExeception e) {
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
			log.error(e.getMessage());
		}
		log.info("Exist from showEdit method");
		
		return page;
	}
	
	@PostMapping("update")
	public String updateCategory(@ModelAttribute Category category,
			RedirectAttributes attributes) {
		
		service.updateCategory(category);
		attributes.addAttribute("message", "Category updated");
		
		return "redirect:all";
	}

}
