package in.nit.sd.controller;

import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import in.nit.sd.entity.Product;
import in.nit.sd.service.OrderService;
import in.nit.sd.service.ProductService;
import in.nit.sd.util.OrderUtil;
import in.nit.sd.view.ProductExcelView;

@Controller
@RequestMapping("/sales")
public class SalesReports {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private ServletContext sc;
	
	@Autowired
	private OrderUtil orderUtil;
	
	@GetMapping("/reports")
	public String showReports(Model model) {
		
		long productCount = productService.totalProducts();
		model.addAttribute("products", productCount);
		
		List<Object[]> data = orderService.getOrderStatusAndCount();
		String path = sc.getRealPath("/");
		orderUtil.generatePie(data, path);
		orderUtil.generateBar(data, path);
		
		return "SalesReportsPage";
	}
	
	@GetMapping("/exportProducts")
	public ModelAndView exportProducts() {
		
		List<Product> products = productService.getAllProducts();
		ModelAndView mv = new ModelAndView();
		mv.addObject("list", products);
		mv.setView(new ProductExcelView());
		
		
		
		return mv;
	}
	

}
