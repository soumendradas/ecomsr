package in.nit.sd.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import in.nit.sd.constant.OrderStatus;
import in.nit.sd.service.OrderService;

@Controller
@RequestMapping("/payment")
public class PaymentController {
	
	@Autowired
	private OrderService service;
	
	@GetMapping("billPay")
	public String doPayment(@RequestParam Long id) {
		
		service.updateOrderStatus(id, OrderStatus.PLACED);
		
		return "redirect:/order/customerOrders";
	}

}
