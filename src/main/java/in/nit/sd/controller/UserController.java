package in.nit.sd.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.sd.constant.MyStatus;
import in.nit.sd.constant.UserRoles;
import in.nit.sd.entity.Customer;
import in.nit.sd.entity.User;
import in.nit.sd.service.CartItemService;
import in.nit.sd.service.CustomerService;
import in.nit.sd.service.UserService;
import in.nit.sd.util.AppUtil;

@Controller
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService service;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private CartItemService itemService;
	
	@GetMapping("register")
	public String showRegisterPage(
			@RequestParam(value = "message", required = false) String message,
			Model model) {
		
		model.addAttribute("message", message);
		
		return "UserRegister";
	}
	
	@PostMapping("save")
	public String saveUser(@ModelAttribute User user,
			RedirectAttributes attributes) {
		user.setStatus(MyStatus.INACTIVE);
		Long id = service.saveUser(user);
		String message = "User created with ID: "+id;
		System.out.println(user.getPassword());
		attributes.addAttribute("message", message);
		return "redirect:register";
	}
	
	@GetMapping("all")
	public String showAllUser(Model model, 
			@RequestParam(value = "message", required = false) String message) {
		List<User> users = service.getAllUser();
		model.addAttribute("list", users);
		model.addAttribute("message", message);
		return "UserData";
	}
	
	@GetMapping("active")
	public String activateUser(@RequestParam Long id,
			RedirectAttributes attributes) {
		
		String message = "";
		
		try {
			service.updateStatus(id, MyStatus.ACTIVE);
			message= "User ID: "+id+" activated";
		}catch (Exception e) {
			message = e.getMessage();
		}
		
		attributes.addAttribute("message", message);
		
		return "redirect:all";
	}
	
	@GetMapping("inactive")
	public String inActiveUser(@RequestParam Long id,
			RedirectAttributes attributes) {
		String message = "";
		try {
			service.updateStatus(id, MyStatus.INACTIVE);
			message = "User Inactivated";
		}catch (Exception e) {
			message = e.getMessage();
		}
		
		attributes.addAttribute("message", message);
		return "redirect:all";
	}
	
	//EDIT BY ADMIN
	@GetMapping("edit")
	public String showEditPage(@RequestParam Long id,
			Model model, RedirectAttributes attributes) {
		
		String page = "";
		
		try {
			User user = service.getOneUser(id);
			model.addAttribute("user", user);
			page = "UserEdit";
		}catch (Exception e) {
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		
		return page;
	}
	
	@PostMapping("update")
	public String udpateUserByAdmin(@ModelAttribute User user,
			RedirectAttributes attributes) {
		String message = "";
		try {
			service.updateByAdmin(user);
			message = user.getDisplayName()+" is updated with id "+ user.getId();
		}catch (Exception e) {
			message = e.getMessage();
		}
		
		attributes.addAttribute("message", message);
		return "redirect:all";
	}
	
	
	@GetMapping("/setup")
	public String setupUser(HttpSession session) {
		String page = "";
		
		@SuppressWarnings("unchecked")
		String role = ((List<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities()).get(0).getAuthority();
		String email = SecurityContextHolder.getContext()
				.getAuthentication().getName();
		
		User user = service.findByEmail(email);
		session.setAttribute("userOb", user);
		
		System.out.println(session.getAttribute("userOb"));
		
		if(role.equals(UserRoles.ADMIN.name())) {
			page = "/user/all";
		}else if(role.equals(UserRoles.SALES.name())) {
			page = "/product/all";
		}else if(role.equals(UserRoles.EMPLOYEE.name())) {
			page = "/brand/all";
		}else if(role.equals(UserRoles.CUSTOMER.name())) {
			Customer cust = customerService.findByEmail(email);
			session.setAttribute("customer", cust);
			session.setAttribute("cartItemCount", itemService.getCartItemCount(cust.getId()));
			page = "/search/";
		}
		
		return "redirect:"+page;
	}
	
	@GetMapping("login")
	public String showLoginPage() {
		
		return "UserLogin";
	}
	
	@GetMapping("checkEmail")
	@ResponseBody
	public String checkEmail(@RequestParam Long id,
			@RequestParam String email) {
		
		String message = "";
		
		if(service.checkEmail(id, email)) {
			message = "Email already exists";
		}
		
		return message;
	}
	
	
	
}
