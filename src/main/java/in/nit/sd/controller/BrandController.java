package in.nit.sd.controller;

import java.util.List;

import org.apache.logging.log4j.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.sd.entity.Brand;
import in.nit.sd.service.BrandService;

@Controller
@RequestMapping("/brand")
public class BrandController {
	
	@Autowired
	private BrandService service;
	
	@GetMapping("register")
	public String showRegisterPage(
			@RequestParam(value = "message", required = false) String message,
			Model model) {
		
		model.addAttribute("message", message);
		
		return "BrandRegister";
	}
	
	@PostMapping("save")
	public String saveBrand(@ModelAttribute Brand brand,
			RedirectAttributes attributes) {
		
		Long id = service.saveBrand(brand);
		String message = "Brand added with ID: "+id;
		attributes.addAttribute("message",message);
		return "redirect:register";
	}
	
	@GetMapping("all")
	public String showAllBrand(Model model,
			@RequestParam(value = "message", required = false) String message) {
		
		List<Brand> list = service.getAllBrand();
		model.addAttribute("list",list);
		model.addAttribute("message", message);
		
		return "BrandData";
	}
	
	@GetMapping("delete")
	public String deleteBrand(@RequestParam Long id,
			RedirectAttributes attributes) {
		
		try {
			service.deleteBrand(id);
			
			attributes.addAttribute("message", "Brand ID:"+id+" is deleted ");
		}catch (Exception e) {
			attributes.addAttribute("message", e.getMessage());
		}
		
		return "redirect:all";
	}
	
	@GetMapping("edit")
	public String showEditPage(@RequestParam Long id, Model model,
			RedirectAttributes attributes) {
		
		String page = "";
		
		try {
		Brand brand = service.getOneBrand(id);
		model.addAttribute("brand", brand);
		page = "BrandEdit";
		}catch (Exception e) {
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		
		return page;
	}
	
	@PostMapping("update")
	public String updateBrand(@ModelAttribute Brand brand,
			RedirectAttributes attributes) {
		
		String message = service.updateBrand(brand);
		attributes.addAttribute("message", message);
		
		return "redirect:all";
	}
	
	@GetMapping("checkName")
	@ResponseBody
	public String validateBrandName(@RequestParam Long id, @RequestParam String name) {
		
		String message = "";
		if(service.isBrandNameExist(id, name)) {
			message = name + ", already exist";
		}
		
		return message;
	}
	
	@GetMapping("checkCode")
	@ResponseBody
	public String validateBrandCode(@RequestParam Long id, @RequestParam String code) {
		String message = "";
		
		if(service.isBrandNameExist(id, code)) {
			message = code+", is exist";
		}
		
		return message;
	}

}
