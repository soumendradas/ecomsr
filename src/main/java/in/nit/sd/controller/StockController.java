package in.nit.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.sd.entity.Stock;
import in.nit.sd.service.ProductService;
import in.nit.sd.service.StockService;

@Controller
@RequestMapping("stock")
public class StockController {
	
	@Autowired
	private StockService service;
	
	@Autowired
	private ProductService productService;
	
	private void commonUi(Model model) {
		model.addAttribute("products", productService.getAllProductIdAndName());
	}

	@GetMapping("register")
	public String showRegisterPage(Model model,
			@RequestParam(value = "message", required = false) String message) {
		model.addAttribute("message", message);
		commonUi(model);
		return "StockRegister";
	}
	
	@PostMapping("save")
	public String saveStock(@ModelAttribute Stock stock,
			RedirectAttributes attributes) {
		String message = "";
		Long id = service.getStockIdByProduct(stock.getProduct().getId());
		System.out.println(id);
		if(id!=null){
			service.updateStock(id, stock.getCount());
			message = "Stock ID: "+id+" is updated";
			
		}else {
			
			Long new_id = service.createStock(stock);
			message = "Stock created with ID: "+new_id;
		}
		
		attributes.addAttribute("message", message);
		
		return "redirect:register";
	}
	
	@GetMapping("all")
	public String showAllStock(Model model) {
		
		List<Stock> list = service.getStockDetails();
		model.addAttribute("list", list);
		return "StockData";
	}
	
	@GetMapping("edit")
	public String showEditPage(@RequestParam Long id,
			Model model,
			RedirectAttributes attributes) {
		
		String page = "";
		try {
			Stock stock = service.getOneStock(id);
			model.addAttribute("stock", stock);
			commonUi(model);
			page = "StockEdit";
		}catch (Exception e) {
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		
		return page;
	}
	
	@PostMapping("update")
	public String updateStock(@ModelAttribute Stock stock,
			RedirectAttributes attributes) {
		String message = "";
		try {
			service.updateStock(stock.getId(), stock.getCount());
			message = "Stock ID: "+stock.getId()+" is updated";
		}catch (Exception e) {
			message= e.getMessage();
		}
		attributes.addAttribute("message", message);
		
		return "redirect:all";
	}
}
