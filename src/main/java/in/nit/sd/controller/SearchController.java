package in.nit.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import in.nit.sd.entity.Category;
import in.nit.sd.entity.CategoryType;
import in.nit.sd.entity.Product;
import in.nit.sd.service.BrandService;
import in.nit.sd.service.CategoryService;
import in.nit.sd.service.CategoryTypeService;
import in.nit.sd.service.ProductService;

@Controller
@RequestMapping({"/search", "/"})
public class SearchController {

	@Autowired
	private BrandService brandService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategoryTypeService typeService;
	
	@Autowired
	private CategoryService catService;
	
	@GetMapping("/")
	public String showBrands(Model model) {
		List<Object[]> list = brandService.getBrandIdAndImageLink();
		model.addAttribute("list", list);
		
		return "BrandPage";
	}
	
	@GetMapping("/productsByBrand")
	public String showProductsByBrands(@RequestParam Long brandId,
			Model model) {
		
		List<Object[]> list = productService.getProductByBrandId(brandId);
		
		model.addAttribute("list", list);
		
		return "ProductPage";
	}
	
	@GetMapping("/type")
	public String showAllCategoryType(Model model) {
		
		List<CategoryType> list = typeService.getAllCategoryTypes();
		model.addAttribute("list", list);
		
		return "CategoryTypePage";
	}
	
	
	@GetMapping("getCategories")
	public String showAllCategoriesByType(@RequestParam Long typeId,
			Model model) {
		List<Category> list = catService.getCategoryByType(typeId);
		model.addAttribute("list", list);
		
		return "CategoryPage";
	}
	
	@GetMapping("categoryProducts")
	public String showAllProductsByCategory(@RequestParam Long catId, 
			Model model) {
		
		List<Object[]> list = productService.getProductByCategoryId(catId);
		model.addAttribute("list", list);
		return "ProductPage";
	}
	
	@GetMapping("byProduct")
	public String showAllProductsByMatchingName(@RequestParam String productName,
			Model model) {
		
		List<Object[]> list = productService.getProductByMatchingName(productName);
		
		model.addAttribute("list",list);
		
		return "ProductPage";
	}
	
	@GetMapping("/productView")
	public String showProductById(@RequestParam Long prodId,
			Model model,
			@RequestParam(value = "message", required = false) String message){
		Product p = productService.getOneProduct(prodId);
		model.addAttribute("pob", p);
		model.addAttribute("message", message);
		return "ProductViewPage";
	}
	
}
