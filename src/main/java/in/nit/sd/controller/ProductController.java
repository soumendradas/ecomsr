package in.nit.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import aj.org.objectweb.asm.Attribute;
import in.nit.sd.entity.Product;
import in.nit.sd.service.BrandService;
import in.nit.sd.service.CategoryService;
import in.nit.sd.service.ProductService;

@Controller
@RequestMapping("product")
public class ProductController {
	
	@Autowired
	private ProductService service;
	
	@Autowired
	private CategoryService catService;
	
	@Autowired
	private BrandService brandService;
	
	private void sendData(Model model) {
		model.addAttribute("brands", brandService.getAllBrandNameAndId());
		model.addAttribute("cats", catService.getAllCategoryIdAndName());
	}
	
	@GetMapping("register")
	public String showRegisterPage(Model model, 
			@RequestParam(value = "message", required = false) String message) {
		
		sendData(model);
		model.addAttribute("message", message);
		
		return "ProductRegister";
	}
	
	@PostMapping("save")
	public String saveProduct(@ModelAttribute Product product,
			RedirectAttributes attributes) {
		Long id = service.saveProduct(product);
		String message = "Product created with id: "+id;
		attributes.addAttribute("message", message);
		
		return "redirect:register";
	}
	
	@GetMapping("all")
	public String showAll(Model model, 
			@RequestParam(value = "message", required = false) String message) {
		
		List<Product> list = service.getAllProducts();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "productData";
	}
	
	@GetMapping("delete")
	public String deleteProduct(@RequestParam Long id, 
			RedirectAttributes attributes) {
		String message = null;
		try {
			service.deleteProduct(id);
			message = "Product Deleted with ID: "+ id;
		}catch (Exception e) {
			message = e.getMessage();
		}
		attributes.addAttribute("message", message);
		return "redirect:all";
	}
	
	@GetMapping("/edit")
	public String showEditPage(@RequestParam Long id,
			Model model, RedirectAttributes attributes) {
		
		String page = null;
		
		try {
			Product product = service.getOneProduct(id);
			model.addAttribute("product", product);
			sendData(model);
			page = "ProductEdit";
		}catch (Exception e) {
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		
		return page;
	}
	
	@PostMapping("update")
	public String updateProduct(@ModelAttribute Product product,
			RedirectAttributes attributes) {
		String message = null;
		try {
			service.updateProduct(product);
			message = "Product updated with ID: "+ product.getId();
		}catch (Exception e) {
			message = e.getMessage();
		}
		attributes.addAttribute("message", message);
		return "redirect:all";
	}
}
