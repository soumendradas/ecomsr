package in.nit.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.sd.entity.CategoryType;
import in.nit.sd.exception.CategoryTypeExeception;
import in.nit.sd.service.CategoryTypeService;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/categorytype")
@Slf4j
public class CategoryTypeController {
	
	@Autowired
	private CategoryTypeService service;
	
	@GetMapping("/register")
	public String registerCategoryType(Model model,
			@RequestParam(value = "message", required = false) String message) {
		model.addAttribute("categorytype",new CategoryType());
		model.addAttribute("message", message);
		return "CategoryTypeRegister";
	}

	@PostMapping("/save")
	public String saveCategoryType(@ModelAttribute CategoryType categorytype, 
			RedirectAttributes attributes) {
		log.info("Enter to save method");
		try {
			Long id=service.saveCategoryType(categorytype);
			attributes.addAttribute("message","CategoryType created with Id:"+id);
			log.debug("CategoryType created with Id:"+id);
		}catch (Exception e) {
			log.error(e.getMessage());
		}
		log.info("Exist from save method");
		return "redirect:register";
	}

	@GetMapping("/all")
	public String getAllCategoryTypes(Model model,
			@RequestParam(value = "message", required = false) String message) {
		List<CategoryType> list=service.getAllCategoryTypes();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "CategoryTypeData";
	}

	@GetMapping("/delete")
	public String deleteCategoryType(@RequestParam Long id, RedirectAttributes attributes) {
		log.info("Enter to delete method");
		try {
			service.deleteCategoryType(id);
			attributes.addAttribute("message","CategoryType deleted with Id:"+id);
			log.debug("CategoryType deleted with Id:"+id);
		} catch(CategoryTypeExeception e) {
			e.printStackTrace() ;
			log.error(e.getMessage());
			attributes.addAttribute("message",e.getMessage());
		}
		
		log.info("Exist from delete method");
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editCategoryType(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page=null;
		log.info("Enter to showEdit page");
		try {
			CategoryType ob=service.getOneCategoryType(id);
			model.addAttribute("categorytype",ob);
			page="CategoryTypeEdit";
			log.debug("Get item successfully");
		} catch(CategoryTypeExeception e) {
			e.printStackTrace() ;
			log.error(e.getMessage());
			attributes.addAttribute("message",e.getMessage());
			page="redirect:all";
		}
		log.info("Exist from showEdit method");
		return page;
	}

	@PostMapping("/update")
	public String updateCategoryType(@ModelAttribute CategoryType categorytype,
			RedirectAttributes attributes) {
		service.updateCategoryType(categorytype);
		attributes.addAttribute("message","CategoryType updated");
		return "redirect:all";
	}

	

}
