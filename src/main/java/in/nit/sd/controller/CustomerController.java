package in.nit.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.sd.entity.Customer;
import in.nit.sd.service.CustomerService;

@Controller
@RequestMapping("customer")
public class CustomerController {
	
	@Autowired
	private CustomerService service;
	
	@GetMapping("register")
	public String showRegisterPage(Model model,
			@RequestParam(value = "message", required = false) String message) {
		model.addAttribute("message", message);
		return "CustomerRegister";
	}
	
	@PostMapping("save")
	public String saveCustomer(@ModelAttribute Customer customer,
			RedirectAttributes attributes) {
		Long id = service.saveCustomer(customer);
		
		String message = "Customer registered successfull with ID: "+id;
		attributes.addAttribute("message", message);
		
		return "redirect:register";
	}
	
	@GetMapping("all")
	public String showAllCustomer(Model model,
			@RequestParam(value = "message", required = false) String message) {
		
		List<Customer> list = service.getAllCustomers();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		
		return "CustomerData";
	}
}
