package in.nit.sd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.sd.entity.Shipping;
import in.nit.sd.service.ShippingService;

@Controller
@RequestMapping("shipping")
public class ShippingController {
	
	@Autowired
	private ShippingService service;
	
	@GetMapping("register")
	public String showRegisterPage(Model model,
			@RequestParam(value = "message", required = false)String message) {
		
		model.addAttribute("message", message);
		
		return "ShippingRegister";
	}
	
	@PostMapping("save")
	public String saveShipping(@ModelAttribute Shipping shipping,
			RedirectAttributes attributes) {
		
		Long id = service.saveShipping(shipping);
		
		attributes.addAttribute("message", "Shipping created with id "+ id);
		
		return "redirect:register";
	}
	
	@GetMapping("all")
	public String getAll(Model model, 
			@RequestParam(value = "message", required = false)String message) {
		
		List<Shipping> list = service.getAllShipping();
		
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "ShippingData";
	}
	
	@GetMapping("delete")
	public String deleteShipping(@RequestParam Long id,
			RedirectAttributes attributes) {
		String message = "";
		try {
			service.deleteShipping(id);;
			message = "Shipping deleted "+id;
		}catch (Exception e) {
			message = e.getMessage();
		}
		attributes.addAttribute("message", message);
		return "redirect:all";
	}
	
	@GetMapping("edit")
	public String showEditPage(@RequestParam Long id, Model model,
			RedirectAttributes attributes) {
		String page = "";
		try {
			Shipping shipping = service.getOneShipping(id);
			model.addAttribute("shipping", shipping);
			page = "ShippingEdit";
		}catch (Exception e) {
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		
		return page;
	}
	
	@PostMapping("update")
	public String updateShipping(@ModelAttribute Shipping shipping,
			RedirectAttributes attributes) {
		
		String message = null;
		try {
			service.updateShipping(shipping);
			message = "Shipping updated";
		}catch (Exception e) {
			message = e.getMessage();
		}
		
		attributes.addAttribute("message", message);
		return "redirect:all";
	}

}
