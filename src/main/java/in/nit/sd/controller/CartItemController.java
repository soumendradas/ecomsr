package in.nit.sd.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nit.sd.entity.CartItem;
import in.nit.sd.entity.Customer;
import in.nit.sd.service.CartItemService;
import in.nit.sd.service.ProductService;
import in.nit.sd.util.AppUtil;

@Controller
@RequestMapping("cart")
public class CartItemController {

	
	@Autowired
	private CartItemService service;
	
	@Autowired
	private ProductService productService;
	
	
	@GetMapping("/add")
	public String addItemToCart(@RequestParam Long prodId,
			Principal p,
			HttpSession session,
			RedirectAttributes attributes) {
		
		String message ="";
		
		if(AppUtil.isUserLoggedIn(p)) {
			Customer cust = (Customer) session.getAttribute("customer");
			
			CartItem item = service.getOneCartItem(cust.getId(), prodId);
			
			if(item == null) {
				CartItem newItem = new CartItem();
				newItem.setCustomer(cust);
				newItem.setProduct(productService.getOneProduct(prodId));
				newItem.setQty(1);
				service.addCartItem(newItem);
				message = "Product added to cart....";
				Integer count = (Integer) session.getAttribute("cartItemCount");
				count = count + 1;
				session.setAttribute("cartItemCount", count);
			}else {
				service.updateQty(item.getId(), 1);
				message = "Product quantity increased...";
			}
			
			
		}else {
			
			message = "User must login to user cart";
			
		}
		attributes.addAttribute("message", message);
		attributes.addAttribute("prodId", prodId);
		return "redirect:/search/productView";
	}
	
	@GetMapping("/all")
	public String showCartItems(HttpSession session,
			Model model,
			@RequestParam(value = "message", required = false) String message) {
		
		Customer cust = (Customer) session.getAttribute("customer");
		List<CartItem> items = service.viewAllItems(cust.getId());
		model.addAttribute("list", items);
		model.addAttribute("message", message);
		return "CartItemPage";
	}
	
	
	@GetMapping("remove")
	public String removeCartItem(@RequestParam Long itemId,
			RedirectAttributes attributes,
			HttpSession session) {
		service.removeCartitem(itemId);
		Integer count = (Integer) session.getAttribute("cartItemCount");
		count = count-1;
		session.setAttribute("cartItemCount", count);
		
		return "redirect:all";
	}
	
	@GetMapping("increase")
	public String increaseCartItem(@RequestParam Long itemId) {
		service.updateQty(itemId, 1);
		
		return "redirect:all";
	}
	
	@GetMapping("decrease")
	public String decreaseCartItem(@RequestParam Long itemId) {
		
		service.updateQty(itemId, -1);
		
		return "redirect:all";
	}
}
