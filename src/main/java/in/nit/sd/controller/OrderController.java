package in.nit.sd.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import in.nit.sd.constant.OrderStatus;
import in.nit.sd.entity.CartItem;
import in.nit.sd.entity.Customer;
import in.nit.sd.entity.Order;
import in.nit.sd.service.CartItemService;
import in.nit.sd.service.CustomerService;
import in.nit.sd.service.OrderService;
import in.nit.sd.util.OrderUtil;

@Controller
@RequestMapping("order")
public class OrderController {
	
	@Autowired
	private OrderService service;
	
	@Autowired
	private CartItemService itemService;
	
	@Autowired
	private CustomerService custService;
	
	@Autowired
	private OrderUtil orderUtil;
	
	@Autowired
	private ServletContext sc;
	
	
	@GetMapping("/create")
	public String placeOrder(HttpSession session) {
		
		Customer customer = (Customer) session.getAttribute("customer");
		
		List<CartItem> cartItems = itemService.viewAllItems(customer.getId());
		Order order = orderUtil.MapCartItemsToOrder(cartItems);
		order.setCustomer(customer);
		service.placeOrder(order);
		itemService.deleteAllCartItems(cartItems);
		session.setAttribute("cartItemCount", 0);
		
		return "redirect:customerOrders";
	}
	
	@GetMapping("customerOrders")
	public String getCustomerOrders(HttpSession session,
			Model model) {
		
		Customer cust = (Customer) session.getAttribute("customer");
		
		List<Order> orders = service.getOrdersByCustomerId(cust.getId());
		orderUtil.calculateGrandTotal(orders);
		model.addAttribute("list", orders);
		model.addAttribute("custAddress", custService.getCustomerAddress(cust.getId()).get(0));
		//TODO Work not done please review it
		return "CustomerOrderPage";
	}
	
	@GetMapping("/all")
	public String getAllOrders(Model model) {
		List<Order> list = service.getAllOrder();
		
		model.addAttribute("list", list);
		return "OrderDataPage";
	}
	


	@GetMapping("/updateOrderStatus")
	public String updateOrderStatus(@RequestParam Long id,
			@RequestParam OrderStatus status) {
		
		service.updateOrderStatus(id, status);
		return "redirect:all";
	}
	
	@GetMapping("/reports")
	public String showReports() {
		
		List<Object[]> data = service.getOrderStatusAndCount();
		String path = sc.getRealPath("/");
		orderUtil.generatePie(data, path);
		orderUtil.generateBar(data, path);
		
		return "SalesReport";
	}
	

}
