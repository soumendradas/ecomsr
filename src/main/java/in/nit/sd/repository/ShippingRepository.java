package in.nit.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nit.sd.entity.Shipping;

public interface ShippingRepository extends JpaRepository<Shipping, Long> {

}
