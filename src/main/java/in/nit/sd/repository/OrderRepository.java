package in.nit.sd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import in.nit.sd.constant.OrderStatus;
import in.nit.sd.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{

	@Query("UPDATE Order SET status=:status WHERE id=:id")
	@Modifying
	void updateOrderStatus(Long id, OrderStatus status);
	
	@Query("SELECT od FROM Order AS od INNER JOIN od.customer as cust WHERE cust.id=:custId")
	List<Order> getOrdersByCustomer(Long custId);
	
	List<Order> findByStatus(OrderStatus status);
	
	@Query("SELECT status, COUNT(status) FROM Order GROUP BY status")
	List<Object[]> getOrderStatusAndCount();
}
