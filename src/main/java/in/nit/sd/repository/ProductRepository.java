package in.nit.sd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.nit.sd.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query("SELECT id, name FROM Product WHERE status=:status")
	List<Object[]> getAllIdAndName(String status);
	
	@Query("SELECT p.id, p.imageLink, p.name, p.shortDesc, p.cost FROM Product as p INNER JOIN p.brand as b WHERE b.id=:brandId")
	List<Object[]> getAllProductByBrand(Long brandId);
	
	@Query("SELECT p.id, p.imageLink, p.name, p.shortDesc, p.cost FROM Product as p INNER JOIN p.category AS c WHERE c.id =:catId")
	List<Object[]> getAllProductByCategory(Long catId);
	
	@Query("SELECT p.id, p.imageLink, p.name, p.shortDesc, p.cost FROM Product as p WHERE p.name LIKE CONCAT('%',:name,'%')")
	List<Object[]> getProductBymatchingName(String name);
}
