package in.nit.sd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.nit.sd.entity.Brand;

public interface BrandRepository extends JpaRepository<Brand, Long> {

	@Query("SELECT id, name FROM Brand")
	List<Object[]> getAllBrandIdAndName();
	
	@Query("SELECT COUNT(name) FROM Brand WHERE name=:name")
	Long getBrandNameCount(String name);
	
	@Query("SELECT COUNT(name) FROM Brand WHERE id!=:id AND name=:name")
	Long getBrandNameCountForEdit(Long id, String name);
	
	@Query("SELECT COUNT(code) FROM Brand WHERE code=:code")
	Long getBrandCodeCount(String code);
	
	@Query("SELECT COUNT(code) FROM Brand WHERE id!=:id AND code = :code")
	Long getBrandCodeCountForEdit(Long id, String code);
	
	@Query("SELECT id, imageLink FROM Brand")
	List<Object[]> getBrandIdAndImageLink();
	
}
