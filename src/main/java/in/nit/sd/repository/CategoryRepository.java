package in.nit.sd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.nit.sd.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

	@Query("SELECT id, name FROM Category WHERE status='ACTIVE'")
	List<Object[]> getAllCategoryIdAndName();
	
	@Query("SELECT c FROM Category c INNER JOIN c.categoryType as cat WHERE cat.id = :typeID")
	List<Category> getCategoryByCategoryType(Long typeID);
}
