package in.nit.sd.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import in.nit.sd.entity.CartItem;

public interface CartItemRepository extends JpaRepository<CartItem, Long> {
	
	@Query("SELECT c FROM CartItem AS c INNER JOIN c.customer AS cust WHERE cust.id=:custId")
	List<CartItem> getAllCartItem(Long custId);
	
	@Modifying
	@Query("UPDATE CartItem SET qty=qty+:newQty WHERE id=:cartId")
	void updateQty(Long cartId, Integer newQty);
	
	@Query("SELECT c FROM CartItem AS c INNER JOIN c.customer AS cust INNER JOIN c.product AS prod WHERE cust.id=:custId AND prod.id=:prodId")
	Optional<CartItem> getOneCartItemByCustIdAndProdId(Long prodId, Long custId);
	
	@Query("SELECT COUNT(c) FROM CartItem AS c INNER JOIN c.customer AS cust WHERE cust.id =:custId")
	int getCartItemsCount(Long custId);
}
