package in.nit.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nit.sd.entity.Coupon;

public interface CouponRepository extends JpaRepository<Coupon, Long> {

}
