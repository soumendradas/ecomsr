package in.nit.sd.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.nit.sd.entity.Address;
import in.nit.sd.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	Optional<Customer> findByEmail(String email);
	
	@Query("SELECT c.address FROM Customer as c WHERE c.id=:custId")
	List<Address> getCustomerAddresses(Long custId);
}
