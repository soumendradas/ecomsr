package in.nit.sd.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import in.nit.sd.constant.MyStatus;
import in.nit.sd.constant.UserRoles;
import in.nit.sd.entity.User;


public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByEmail(String email);
	
	@Modifying
	@Query("UPDATE User SET status=:ms WHERE id=:id")
	void updateStatus(Long id, MyStatus ms);
//	
	@Modifying
	@Query("UPDATE User SET displayName=:displayName, email=:email, roles=:roles, address=:address WHERE id=:id")
	void updateByAdmin(Long id, String displayName, String email, UserRoles roles, String address );
	
	
	@Query("SELECT COUNT(email) FROM User WHERE email=:email")
	Long countEmail(String email);
	
	@Query("SELECT COUNT(email) FROM User WHERE email=:email AND id !=:id")
	Long countEmail(Long id, String email);
}
