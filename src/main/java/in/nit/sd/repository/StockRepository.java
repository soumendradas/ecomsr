package in.nit.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import in.nit.sd.entity.Stock;

public interface StockRepository extends JpaRepository<Stock, Long> {

	@Query("SELECT s.id FROM Stock AS s INNER JOIN s.product as prod WHERE prod.id=:productId")
	Long getStockIdByProductId(Long productId);
	
	@Modifying
	@Query("UPDATE Stock SET qoh = qoh+:count WHERE id=:id")
	void updateStock(Long id, Integer count);
}
