package in.nit.sd.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "address_tab")
@Data
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="hno_col")
	private Integer houseNo;
	
	@Column(name = "loc_col")
	private String location;
	
	@Column(name = "city_col")
	private String city;
	
	@Column(name = "state_col")
	private String state;
	
	@Column(name = "country_col")
	private String country;
	
	@Column(name = "pin_col")
	private Integer pincode;
	
}
