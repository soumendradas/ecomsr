package in.nit.sd.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import in.nit.sd.constant.OrderStatus;
import lombok.Data;

@Entity
@Data
@Table(name = "order_tab")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "order_fk_col")
	private List<OrderItem> orderItems;
	
	@ManyToOne
	@JoinColumn(name = "customer_fk_col")
	private Customer customer;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "order_status_col")
	private OrderStatus status;
	
	@Transient
	private Double grandTotal;
}
