package in.nit.sd.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Table(name = "coupon_tab")
@Data
public class Coupon {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "coup_id_col")
	private Long id;
	
	@Column(name = "coup_code_col", unique = true, nullable = false)
	private String coupCode;
	
	@Column(name = "coup_note_col")
	private String coupNote;
	
	@Column(name="coup_percent_col")
	private Integer coupPercent;
	
	@Column(name = "coup_available_col")
	private String coupAvailable;
	
	@Column(name = "coup_total_allowed_col")
	private Integer coupTotalAllowed;
	
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	@Column(name = "coup_exp_date_col")
	private LocalDate coupExpDate;
	
	@Column(name = "coup_claimed_count_col")
	private Integer coupClaimedCount;
}
