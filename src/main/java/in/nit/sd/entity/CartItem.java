package in.nit.sd.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "cart_item_tab")
public class CartItem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "key_tab")
	@TableGenerator(name = "key_tab",
					pkColumnName = "key_name",
					pkColumnValue = "cart_item_id",
					valueColumnName = "key_value")
	@Column(name = "cart_id_col")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "product_fk_col")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "customer_fk_col")
	private Customer customer;
	
	@Column(name = "cart_qty_col")
	private Integer qty;

}
