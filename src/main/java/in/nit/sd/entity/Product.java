package in.nit.sd.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "product_tab")
@Data
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "prod_id_col")
	private Long id;
	
	@Column(name = "prod_name_col")
	private String name;
	
	@Column(name = "prod_short_desc_col")
	private String shortDesc;
	
	@Column(name = "prod_full_desc_col")
	private String fullDesc;
	
	@Column(name = "prod_status_col")
	private String status;
	
	@Column(name = "prod_inStock_col")
	private String inStock;
	
	@Column(name = "prod_cost_col")
	private Double cost;
	
	@Column(name = "prod_len_col")
	private Double  len;
	
	@Column(name = "prod_width_col")
	private Double width;
	
	@Column(name = "prod_height_col")
	private Double height;
	
	@Column(name = "prod_dim_unit_col")
	private String dimUnit;
	
	@Column(name = "prod_weight_col")
	private Double weight;
	
	@Column(name = "prod_weight_unit_col")
	private String weightUnit;
	
	@Column(name = "prod_note_col")
	private String note;
	
	@Column(name="prod_image_link_col")
	private String imageLink;
	
	@ManyToOne
	@JoinColumn(name = "brand_fk_col")
	private Brand brand;
	
	@ManyToOne
	@JoinColumn(name = "category_fk_col")
	private Category category;

}
