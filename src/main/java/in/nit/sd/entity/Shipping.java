package in.nit.sd.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import lombok.Data;

@Entity
@Table(name = "shipping_tab")
@Data
public class Shipping {
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "key_tab")
	@TableGenerator(name = "key_tab",pkColumnName = "key_name",
				pkColumnValue = "shipping_pk", valueColumnName = "key_value")
	@Column(name = "ship_id_col")
	private Long id;
	
	@Column(name = "ship_type_col")
	private String shipType;
	@Column(name = "ship_code_col")
	private String shipCode;
	@Column(name = "ship_name_col")
	private String shipName;
	@Column(name = "ship_cost_col")
	private Double shipCost;
	@Column(name = "ship_weight_col")
	private Double shipWeight;
	@Column(name = "ship_weight_type_col")
	private String shipWeightType;
	@Column(name = "ship_note_col")
	private String note;

}
