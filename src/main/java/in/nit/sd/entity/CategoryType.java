package in.nit.sd.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "cat_type_tab")
public class CategoryType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cat_type_name_col")
	private String name;
	
	@Column(name = "cat_type_note_col")
	private String note;
	
	@Column(name = "cat_type_status_col")
	private String status;

}
