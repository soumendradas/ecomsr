package in.nit.sd.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import in.nit.sd.constant.MyStatus;
import in.nit.sd.constant.UserRoles;
import lombok.Data;

@Data
@Entity
@Table(name = "user_tab")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id_col")
	private Long id;
	
	@Column(name = "user_name_col", nullable = false, length = 40)
	private String displayName;
	
	@Column(name = "user_email_col", unique = true, nullable = false, length = 60)
	private String email;
	
	@Column(name = "user_password_col", length = 120, nullable = false)
	private String password;
	
	@Column(name = "user_status_col")
	@Enumerated(EnumType.STRING)
	private MyStatus status;
	
	
	@Enumerated(EnumType.STRING)
	@Column(name = "user_roles_col")
	private UserRoles roles;
	
	@Column(name = "user_address_col")
	private String address;

}
