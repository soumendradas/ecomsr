$(document).ready(function () {
  $("#nameError").hide();
  $("#codeError").hide();
  $("#tagLineError").hide();
  $("#imageError").hide();
  $("#noteError").hide();

  var nameError = false;
  var codeError = false;
  var tagLineError = false;
  var imageError = false;
  var noteError = false;

  function name_validation() {
    var val = $("#name").val();
    var exp = /^[A-Za-z0-9\.\&\@\s]{2,25}$/;

    if (val == "") {
      $("#nameError").show();
      $("#nameError").html("Please enter Brand Name");
      $("#nameError").css("color", "red");
      nameError = false;
    } else if (!exp.test(val)) {
      $("#nameError").show();
      $("#nameError").html("Please enter Valid Name [A-Za-z0-9.&@]");
      $("#nameError").css("color", "red");
      nameError = false;
    } else {
      var id = 0;
      if ($("#id").val() != undefined) {
        id = $("#id").val();
        nameError = true;
      }
      $.ajax({
        url: "checkName",
        data: { id: id, name: val },
        success: function (response) {
          if (response != "") {
            $("#nameError").show();
            $("#nameError").html(response);
            $("#nameError").css("color", "red");
            nameError = false;
          } else {
            $("#nameError").hide();
            nameError = true;
          }
        }
      });
    }
    return nameError;
  }

  function code_validation() {
    var val = $("#code").val();
    var exp = /^[A-Za-z0-9]{2,10}$/;
    if (val == "") {
      $("#codeError").show();
      $("#codeError").html("Please enter Code Name");
      $("#codeError").css("color", "red");
      codeError = false;
    } else if (!exp.test(val)) {
      $("#codeError").show();
      $("#codeError").html("Please enter valid Code [A-Za-z0-9] 10 chars");
      $("#codeError").css("color", "red");
      codeError = false;
    } else {
      var id = 0;
      if ($("#id").val() != undefined) {
        id = $("#id").val();
        codeError = true;
      }

      $.ajax({
        url: "checkCode",
        data: { id: id, code: val },
        success: function (response) {
          if (response != null) {
            $("#codeError").show();
            $("#codeError").html(response);
            $("#codeError").css("color", "red");
            codeError = false;
          } else {
            $("#codeError").hide();
            codeError = true;
          }
        },
      });
    }

    return codeError;
  }

  function tagLine_validation() {
    var val = $("#tagLine").val();
    if (val == "") {
      $("#tagLineError").show();
      $("#tagLineError").html("Please enter TagLine");
      $("#tagLineError").css("color", "red");
      tagLineError = false;
    } else {
      $("#tagLineError").hide();
      tagLineError = true;
    }
    return tagLineError;
  }

  function image_validation() {
    var val = $("#imageLink").val();
    if (val == "") {
      $("#imageError").show();
      $("#imageError").html("Please wait upload Image ");
      $("#imageError").css("color", "red");
      imageError = false;
    } else {
      $("#imageError").hide();
      imageError = true;
    }

    return imageError;
  }

  $("#name").keyup(function () {
    name_validation();
  });

  $("#code").keyup(function () {
    code_validation();
  });

  $("#tagLine").keyup(function () {
    tagLine_validation();
  });

  $("#brand_form").submit(function () {
    name_validation();
    code_validation();
    tagLine_validation();
    image_validation();
    if (nameError && tagLineError && imageError) return true;
    else return false;
  });
});
