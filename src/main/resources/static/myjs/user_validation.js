$(document).ready(function(){
    $("#displayNameError").hide();
    $("#emailError").hide();
    $("#rolesError").hide();
    $("#addressError").hide();

    var displayNameError = false;
    var emailError = false;
    var rolesError = false;
    var addressError = false;


    function name_validation() {
        var val = $("#displayName").val();
        var exp = /^[A-Za-z0-9\s\.\-]{2,30}$/;

        if(val == ""){
            $("#displayNameError").show();
            $("#displayNameError").html("Please enter Name");
            $("#displayNameError").css("color", "red");
            displayNameError = false;
        }else if (!exp.test(val)) {
            $("#displayNameError").show();
            $("#displayNameError").html("Please enter valid name [A-Za-z0-9 space . -] 30 chars");
            $("#displayNameError").css("color", "red");
            displayNameError = false;
        }else{
            $("#displayNameError").hide();
            displayNameError= true;
        }
        return displayNameError;
    }

    function email_validation(){
        var val = $("#email").val();
        var exp = /^[A-Za-z0-9\.\-\&\#]{1,40}[@][A-Za-z0-9\-]{2,15}[.][A-Za-z0-9\-\.]{1,20}$/;
        if(val == ""){
            $("#emailError").show();
            $("#emailError").html("Please Enter Email ID");
            $("#emailError").css("color", "red");
            emailError = false;
        }else if(!exp.test(val)){
            $("#emailError").show();
            $("#emailError").html("Enter valid Email ID [A-Za-z0-9\.@\-\&\#]1-40 chars,@, [A-Za-z0-9\.\-]1-25chars</b>");
            $("#emailError").css("color", "red");
            emailError = false;
        }else{
            
            var id = 0;
            if($("#id").val()!=undefined){
                id = $("#id").val();
                emailError = true;
            }
            $.ajax({
                url: "checkEmail",
                data: { id: id, email: val},
                success: function (response) {
                    if(response!=""){
                        $("#emailError").show();
                        $("#emailError").html(response);
                        $("#emailError").css("color", "red");
                        emailError = false;
                    }else{
                        $("#emailError").hide();
                        emailError = true;
                    }
                }
            })


           
        }

        return emailError;
    }

    function roles_validation(){
        var len = $("inbox[name='roles']:checked").length;
        if(len == 0){
            $("#rolesError").show();
            $("#rolesError").html("Please Select Role");
            $("#rolesError").css("color", "red");
            rolesError = false;
        }else{
            $("#rolesError").show();
            rolesError = true;
        }

        return rolesError;
    }

    function address_validation(){
        var val = $("#address").val();
        if(val == ""){
            $("#addressError").show();
            $("#addressError").html("Please Enter ADDRESS");
            $("#addressError").css("color", "red");
            addressError = false;
        }else{
            $("#addressError").hide();
            addressError = true;
        }

        return addressError;
    }


    $("#displayName").keyup(function () {
        name_validation();
    })

    $("#email").keyup(function (){
        email_validation();
    })

    $("inbox[name='roles']").click(function(){
        roles_validation();
    })

    $("#address").keyup(function (){
        address_validation();
    })

    $("#userForm").submit(function(){
        name_validation();
        email_validation();
        roles_validation();
        address_validation();
        if(nameError&&emailError&&rolesError&&addressError){
            return true;
        }else return false;        
    });

})